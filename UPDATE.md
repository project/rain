This file contains instructions for updating your Rain-based Drupal site.

Once an Rain is installed, all configuration is "owned" by your site and will be left alone by subsequent updates to this project.

In cases where any manual steps are required to upgrade your instance of Rain, we will provide detailed instructions below under the "Update Instructions" heading.

## Update Instructions

These instructions describe how to update your site to bring it in line with a newer version of Rain.

### Update from 2.x to 3.x

The update from Rain 2.x to 3.x introduced a signifcant change to the organization of the Rain project. In Rain 3.x content features have been moved to a separate project named "rain_features." 

To upgrade to 3.x you would need to run `composer require mediacurrent/rain_features` in order to upgrade to 3.x. See "Removed dependencies" for packages that were removed. If you are using any of these packages you will need to add them manually to your project composer, otherwise Drupal will report an error.

__Important__: any dependencies not being added to project composer will need to be __uninstalled__ prior to upgrading to 3.x.

There have also been a few new modules added which can be enabled but are not required (see below).

#### Added dependencies
* drupal/multiline_config
* drupal/twig_tweak
* drupal/twig_field_value

#### Removed dependencies
* drupal/addtoany
* drupal/allowed_formats
* drupal/ckeditor_media_embed
* drupal/colorbox
* drupal/crop
* drupal/entity
* drupal/focal_point
* drupal/libraries
* drupal/media_entity_actions
* drupal/slick
* drupal/slick_media
* drupal/slick_paragraphs
* drupal/slick_views